#include <list>

enum Cards_t {
    two = 2,
    three,
    four,
    five,
    six,
    seven,
    eight,
    nine,
    ten,
    jack,
    queen,
    king,
    ace
};

enum Colors_t {spades, clubs, diamonds, hearts};

const Cards_t Cards_v[] = {two, three, four, five, six, seven, eight, ten, jack, queen, king, ace};
const Colors_t Colors_v[] = {spades, clubs, diamonds, hearts};

class Card {
public:
	Card();
	Card(Colors_t color, Cards_t value);
	Colors_t get_color();
	Cards_t get_value();

private:
	Cards_t m_value;
	Colors_t m_color;

};


class Deck {
public:
	Deck();
	~Deck();
    void shuffle();
    void add_card(Card* p_card);
    void take_card(Card* p_card);
private:
    std::list <Card*> m_card_list;
    Card m_card_set[52];
};
