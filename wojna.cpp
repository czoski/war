#include "wojna.hpp"
#include <stdio.h>

Cards_t Card::get_value() {
	return m_value;
}

Colors_t Card::get_color() {
	return m_color;
}

Card::Card(Colors_t color, Cards_t value) {
	m_color = color;
	m_value = value;
}

Card::Card() {}

Deck::Deck() {
	for (auto color: Colors_v) {
		for (auto value: Cards_v) {
			Card* card = new Card(color, value);
			m_card_list.push_front(card);
			// printf("%d, %d | ", m_card_list.front()->get_color(), m_card_list.front()->get_value());
		}
	}
	printf("%p:\n", m_card_list.front());
}

Deck::~Deck() {
	std::list<Card*>::iterator it;
	for(it = m_card_list.begin(); it != m_card_list.end(); it++) {
		printf("deleting: %p\n", it);
		// delete m_card_list.front();
	}
}

int main() {
Card c = Card(Colors_t::clubs, Cards_t::two);
printf("%d, %d, %p\n", c.get_value(), c.get_color(), c);
Deck d = Deck();
return 0;
}

